# PixTudio Automated Documentation Generation System
The goal of this project is to provide an automated documentation system for the 2D game engine [PixTudio](http://bitbucket.org/josebagar/pixtudio) 
that simplifies the process of documenting the API and language keywords.

The PixTudioDocGen toolchain ultimately produces a complete static website with a nowadays documentation look-and feel. 
Because of the website being static, it is easy to distribute, or host. 

You can see how it looks like [here](http://pixtudiodocs.dacucar.com).

The tool system consists on several subcomponents:

- The pixtudio-lexer: looks into the C sources of PixTudio and extracts all information relative to the API that PixTudio exportes. This includes function names and parameters, constants defined by the language, keywords and data (structures, variables).
- The symbols-blender: the lexer only identifies symbols, but they come with no description, code example or explanation. 
  The symbols-blender mission is to complement this information. This complementary information (which must be manually written) is stored in a Git version-controlled repository where anyone can make easily make contributions.
  The information is stored in [YAML](http://yaml.org/) syntax because in that way it can potentially be easily used by other applications while still being human-readable.
- The markdown-site-builder: generates a pseudo-website in mark-down syntax that can be compiled with [mkdocs](http://www.mkdocs.org/) into a final static website.

The pixtudio-lexer and a preliminary markdown-site-builder have already been written, producing a documentation site that is useful to have a good overview of the PixTudio API. Future work will take place in different stages:

1. Define the structure of the complementary-information repository and develop the symbols-blender.

2. Finish the work in the markdown-site-builder to take the symbols-blender output into account.

3. Create a simple command-line interface and a python package for easy distribution and integration within PixTudio build-system.

For the moment, there is no cli for PixTudio doc-gen and to use it you will have to run a Python script.

## Set-up & use
You will need Python3 for running PixTudioDocGen.

1. Install mkdocs
```
pip3 install mkdocs
```

2. Clone this repository
```
git clone https://bitbucket.org/dacucar/pixtudiodocgen.git
```

3. Open the [application.py](src/main/pixtudiodocgen/application.py) file inside the _pixtudiodocgen_ folder and modify the following constants so as they point to your PixTudio sources:
```
PIXTUDIO_MODULES_PATH = r'path/to/PixTudio/sources/../modules/'
PIXTUDIO_IDENTIFIERS_PATH = r'path/to/PixTudio/sources/../core/pxtb/src/c_main.c'
```

4. The folder _pixtudiodocgen_ is a Python module and you can run it with:
```
python3 -m pixtudiodocgen
```

5. Navigate to the out/ folder in the root of the repository structure and run mkdocs.
