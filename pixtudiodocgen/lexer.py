# -*- coding: utf-8 -*-
#
#  Copyright 2016 Darío Cutillas Carrillo
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

class Lexer(object):
    def __init__(self, header, footer, *args):
        """Constructor.

           Args:
               *args: the elements that define the behaviour of the lexer.
                   Following elements are available:
                   'Word' : Continue only after 'Word' is found, go back if not.
                   '@' : Capture 1 word, then continue.
                   '@Word' : Capture everything until 'Word' is matched
               
        """
        self.tokens = []
        for a in args:
            if a == None:
                raise ValueError("'None' is not a valid lexer element.")
            elif a.startswith("@"):
                if len(a) > 1:
                    self.tokens.append(CaptureUntilMatch(a[1:]))
                else:
                    self.tokens.append(CaptureWord())    
            else:
                self.tokens.append(MatchWord(a))

        self.ready = False
        self.header = header
        self.footer = footer
        
        self.reset()
    
    def reset(self):
        self.current_token_index = 0
        for t in self.tokens:
            t.reset()
    
    @property
    def current_token(self):
        return self.tokens[self.current_token_index]
        
    @property
    def match_found(self):
        return self.current_token_index == len(self.tokens)
        
    @property
    def match(self):
        return [x.memory for x in self.tokens if x.memory != None]
        
    def consume(self, word):
        if not self.ready:
            if word == self.header:
                self.ready = True
            return 
        else:
            if word == self.footer:
                self.ready = False
                return;
        
        self.current_token.do_work(word)

        if self.current_token.failed:
            self.reset()
        elif self.current_token.completed:
            self.current_token_index += 1
        else:
            pass # Idle until curren_token completes its work

class EventLexer(Lexer):
    def __init__(self, event_handler, header, footer, *tokens):
        super(EventLexer, self).__init__(header, footer, *tokens)
        self.on_match_delegate = event_handler
        
    def consume(self, word):
        if self.match_found:
            self.on_match_delegate(self.match)
            self.reset()
        else:
            super(EventLexer, self).consume(word)        
 
class LexerElement(object):
    """
       
    """
    def reset(self):
        self.failed = False
        self.completed = False
            
class MatchWord(LexerElement):
    def __init__(self, token_to_match):
        self.token = token_to_match
        self.reset()
        
    def do_work(self, word):
        self.failed = (self.token != word)
        self.completed = True
        
    @property
    def memory(self):
        return None
        
class CaptureWord(LexerElement):
    def __init__(self):
        self.reset()
        
    def reset(self):
        super(CaptureWord, self).reset()
        self.capture = None        
            
    def do_work(self, word):
        self.capture = word
        self.completed = True
        self.failed = False
         
    @property
    def memory(self):
        return self.capture
        
class CaptureUntilMatch(LexerElement):
    def __init__(self, end_token):
        self.end_token = end_token
        self.reset()
        
    def reset(self):
        super(CaptureUntilMatch, self).reset()
        self.tokens = []
        
    def do_work(self, word):
        if word == self.end_token:
            self.completed = True
        else:
            self.tokens.append(word)
            
    @property
    def memory(self):
        return " ".join(self.tokens)
