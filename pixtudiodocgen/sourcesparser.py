# -*- coding: utf-8 -*-
#
#  Copyright 2016 Darío Cutillas Carrillo
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

from collections import namedtuple
from .lexer import EventLexer

class Constant(object):
    def __init__(self, name, datatype, origin):
        self.name = name
        self.datatype = datatype
        self.origin = origin

class Function(object):
    """
    
    Returnt ypes {'INT', 'POINTER', 'UNDEFINED', 'FLOAT', 'STRING', 'DWORD', 'BYTE'}
    
    Examples of PARAMS
    SAVE SV++
    LOAD SV++
    FREAD IV++
    FWRITE IV++
    KSORT V++V++
    KSORT V++V++I
    SORT V++I
    SORT V++
    WRITE_VAR IIIIV++
    WRITE_VAR IIIIIV++
    
    Examples of functions with UNDEFINED return type:
    FREWIND
    MESSAGEBOX
    SAY
    SAY_FAST


    """
    
    def __init__(self, name, params, returntype, origin):
        self.name = name
        self.params = params
        self._returntype = returntype
        self.origin = origin
    
    @property
    def returntype(self):
        return self._returntype
        

class Variable(object):
    def __init__(self, name):
        self.name = name

class IdentifiersParser(object):
    def __init__(self, filename):
        self.filename = filename
        self.identifiers = []

        debug = False
        lexer = EventLexer(self._unmarshall_identifier, 'identifier_add', ';', '(', '@', ')')
        with open(filename, 'rb') as file:
            for line in file:
                for word in _prepare_line(line).split():
                    lexer.consume(word)
                    #if word == '"IMPORT"':
                    #    debug = True
                    #if debug:
                    #    print(word)
                    #    print(lexer.ready)
                    #    import pdb; pdb.set_trace()

    def _unmarshall_identifier(self, data):
        Identifier = namedtuple('Identifier', 'name')
        
        try:
            identifier = Identifier(name=data[0][1:-1])
            self.identifiers.append(identifier)
        except Exception as e:
            print('Failed _unmarshalling identifier data {0}'.format(data))        
        
class SymbolsParser(object):
    def __init__(self, filename):
        self.filename = filename
        self._constants = []
        self._functions = []
        self._variables = []

        tokenizers = [EventLexer(self._unmarshall_constant, 'DLCONSTANT',';', '{', '@', ',', '@', ',', '@', '}'),
                      EventLexer(self._unmarshall_variable, 'DLVARFIXUP',';', '{', '@', ',', '@', ',', '@', ',', '@', '}'),
                      EventLexer(self._unmarshall_function, 'DLSYSFUNCS', ';', 'FUNC', '(', '@', ',', '@', ',', '@', ',', '@', ')')]
        
        with open(filename, 'rb', 0) as file:
            for line in file:
                for word in _prepare_line(line).split():
                    for tokenizer in tokenizers:
                        tokenizer.consume(word)
                            
        
    @property
    def constants(self):
        return self._constants
        
    @property
    def functions(self):
        return self._functions
    
    @property
    def variables(self):
        return self._variables

    @property
    def has_symbols(self):
        return self.has_variables \
            or self.has_constants \
            or self.has_functions

    @property
    def has_variables(self):
        return len(self.variables) > 0

    @property
    def has_constants(self):
        return len(self.constants) > 0

    @property
    def has_functions(self):
        return len(self.functions) > 0

    def _unmarshall_constant(self, data):
        """Unmarshall constants data
           
           Example of data:
           * Valid: ['"B_SBLEND"', 'TYPE_DWORD', 'B_SBLEND']
           * Not-valid: ['NULL', '0', '0']
        """
        # datatypes: {'FLOAT', 'DWORD', 'BYTE', 'INT'}
        
        if data[0] == 'NULL':
            return
        
        try:
            name = data[0][1:-1]
            datatype = data[1][len('TYPE_'):]
            origin = data[2]
            self._constants.append(Constant(name, datatype, origin))
        
        except Exception as e:
            print('Failed _unmarshalling constant data {0}'.format(data))

    def _unmarshall_variable(self, data):
        """Unmarshall variables data
        """
        if data[0] == 'NULL':
            return

        try:
            name = data[0][1:-1]
            self._variables.append(Variable(name))
        
        except Exception as e:
            print('Failed _unmarshalling variable data {0}'.format(data))

    def _unmarshall_function_parameters(self, parameters):
        # Parameter types {'I', 'F', 'B', 'V++', 'S', 'W', 'P'} 
        params = parameters.replace('V++', 'V')
        translation = { 'I' : 'INT', 
                        'F' : 'FLOAT', 
                        'B' : 'BOOL', 
                        'V' : 'VARSPACE',
                        'S' : 'STRING',
                        'W' : 'DWORD',
                        'P' : 'POINTER' }
                        
        return [translation[x] for x in params]
        
    def _unmarshall_function(self, data):
        """Unmarshall functions data
        
           Example of data received:
           * Valid: ['"GET_WINDOW_POS"', '"PP"', 'TYPE_INT', 'bgd_get_window_pos']
           * Not-valid: ['0', '0', '0', '0']
        """
        if data[0] in ['0', 'NULL']:
            return
            
        try:
            name = data[0][1:-1]
            params = self._unmarshall_function_parameters(data[1][1:-1])
            datatype = data[2][len('TYPE_'):]
            origin = data[3]
            self._functions.append(Function(name, params, datatype, origin))
        
        except Exception as e:
            print('Failed _unmarshalling function data {0}'.format(data))        

def _prepare_line(line):
    """Append spaces between Language tokens"""

    TOKENS = ['{','}','(',')',',','*','=',']','[','/']

    # TODO: Needs to ensure that they are not inside a literal
    # Otherwise structure fields such as '_render_reserved_._saved_.x'
    # will not be captured.
    
    line_chars = []
    in_brackets = False
    for c in line:
        if chr(c) == '"':
            in_brackets = not in_brackets
            line_chars.append(chr(c))
        elif chr(c) in TOKENS and not in_brackets:
            line_chars.append(' ')
            line_chars.append(chr(c))
            line_chars.append(' ')
        else:
            line_chars.append(chr(c))
    
    return ''.join([x for x in line_chars])
